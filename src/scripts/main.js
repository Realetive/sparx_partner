/*eslint "strict": [2, "global"], "curly": 2*/
'use strict';

// ! Unused yet
// function removeClass( obj, cls ) {
//   var classes = obj.className.split( ' ' );
//   for ( var i = 0; i < classes.length; i++ ) {
//     if ( classes[ i ] === cls ) {
//       classes.splice( i, 1 ); // удалить класс
//       i--;
//     }
//   }
//   obj.className = classes.join( ' ' );
// }

function setAttr( classElement, attrName, attrValue ) {
  var elements = document.getElementsByClassName( classElement );
  if ( elements.length ) {
    [].forEach.call( elements, function( element ) {
      element.setAttribute( attrName, attrValue );
    } );
  }
}

setAttr( 'js__state_disabled', 'disabled', 'disabled' );

// Если заявка зарезервирована (добавлен класс "bid_reserved"),
// устанавлием отрицательный tabindex, чтобы исключить возможность
// выделения с помощью <Tab>
setAttr( 'bid_reserved', 'tabindex', '-1' );

var setFocus = document.getElementsByClassName( 'js__state_focus' );
if ( setFocus.length ) {
  setFocus[ 0 ].focus();
}
