import gulp from 'gulp';
import gulpLoadPlugins from 'gulp-load-plugins';
import browserSync from 'browser-sync';
import del from 'del';
import {stream as wiredep} from 'wiredep';
import compress from 'compression';

const $ = gulpLoadPlugins();
const reload = browserSync.reload;
const packageJson = require( './package.json' );
const testLintOptions = {
  env: {
    mocha: true
  }
};

function lint( files, options ) {
  return () => {
    return gulp.src( files )
      .pipe( reload( { stream: true, once: true } ) )
      .pipe( $.eslint( options ) )
      .pipe( $.eslint.format() )
      .pipe( $.if( !browserSync.active, $.eslint.failAfterError() ) );
  };
}

/*!
 * Gulp plugin to preprocess HTML, JavaScript, and other files based
 * on custom context or environment configuration
 * https://github.com/jas/gulp-preprocess
 */
gulp.task( 'templates', () => {
  return gulp.src( ['src/_templates/**/*.html', '!src/_templates/components/**/*'] )
    .pipe( $.preprocess( { context: packageJson } ) )
    .pipe( gulp.dest( 'src' ) );
});

gulp.task( 'styles', [ 'templates' ], () => {
  return gulp.src( 'src/styles/*.scss' )
    .pipe( $.plumber() )
    .pipe( $.sourcemaps.init() )
    .pipe( $.sass.sync( {
      outputStyle: 'expanded',
      precision: 10,
      includePaths: [ '.' ]
    } ).on( 'error', $.sass.logError) )
    .pipe( $.autoprefixer( { browsers: [ 'last 3 version' ] } ) )
    .pipe( $.sourcemaps.write( './' ) )
    .pipe( gulp.dest( '.tmp/styles' ) )
    .pipe(reload( { stream: true } ) );
});

gulp.task( 'lint', lint( 'src/scripts/**/*.js' ) );
gulp.task( 'lint:test', lint( 'test/spec/**/*.js', testLintOptions ) );

gulp.task( 'html', [ 'styles' ], () => {
  const assets = $.useref.assets( { searchPath: [ '.tmp', 'src', '.' ] } );

  return gulp.src( 'src/*.html' )
    .pipe( $.plumber() )
    .pipe( assets )
    .pipe( $.if( '*.js', $.uglify() ) )
    .pipe( $.if( '*.css', $.minifyCss( { compatibility: '*' } ) ) )
    .pipe( assets.restore() )
    .pipe( $.useref() )
    .pipe( $.if( '*.html', $.minifyHtml( { conditionals: true, loose: true } ) ) )
    .pipe( gulp.dest( 'dist' ) );
});

gulp.task( 'images', [ 'sprites:svg' ], () => {
  return gulp.src( [ 'src/images/**/*', '!src/images/_to-sprites'] )
    .pipe( $.if($.if.isFile, $.cache($.imagemin( {
      progressive: true,
      interlaced: true,
      // don't remove IDs from SVGs, they are often used
      // as hooks for embedding and styling
      svgoPlugins: [ { cleanupIDs: false } ]
    } ) )
    .on( 'error', function ( err ) {
      console.log( err );
      this.end();
    } ) ) )
    .pipe( gulp.dest( 'dist/images' ) );
});

gulp.task( 'sprites:svg', () => {

  let config = {
    dest                  : '.'           // Main output directory
  , log                   : 'info'        // Logging verbosity or custom logger
  , shape                 : {             // SVG shape configuration
      id                  : {               // SVG shape ID related options
          separator       : '_'               // Separator for directory name traversal
        , generator       : 'sprite__%s'      // SVG shape ID generator callback
        , pseudo          : '~'               // File name separator for shape states (e.g. ':hover')
        , whitespace      : '_'               // Whitespace replacement for shape IDs
        }
    , dimension           : {               // Dimension related options
          maxWidth        : 2000              // Max. shape width
        , maxHeight       : 2000              // Max. shape height
        , precision       : 2                 // Floating point precision
        , attributes      : false             // Width and height attributes on embedded shapes
        }
    , spacing             : {               // Spacing related options
        padding           : 0                 // Padding around all shapes
      , box               : 'content'         // Padding strategy (similar to CSS `box-sizing`)
      }
    , transform           : ['svgo']        // List of transformations / optimizations
    , meta                : null            // Path to YAML file with meta / accessibility data
    , align               : null            // Path to YAML file with extended alignment data
    , dest                : 'src'           // Output directory for optimized intermediate SVG shapes
    }
  , svg                   : {             // Sprite SVG options
      xmlDeclaration      : true
    , doctypeDeclaration  : true
    , namespaceIDs        : true
    , namespaceClassnames : true
    , dimensionAttributes : true
    , rootAttributes      : {}
    , precision           : 0
    , transform           : []
    }
  , variables             : {}            // Custom templating variables
  , mode                  : {             // Output mode configurations
      css                 : false
      //  css                 : {               // Activate the css mode
      //    dest              : '../../styles'
      //  , prefix            : '.icon-%s'
      //  , dimensions        : true
      //  , sprite            : '../images/icons/icons.sprite.svg'
      //  , bust              : true
      //  , render            : {
      //      scss            : true                // Activate CSS output (with default options)
      //    }
      //  , example           : {
      //      dest            : '../images/icons/test.sprite.html'
      //    }
      //  , layout            : 'packed'
      //  , common            : 'icon-sprite:before'
      //  , mixin             : 'icon-sprite-mixin'
      //  }
    , view                : false           // Activate the view mode
    , defs                : false           // Activate the defs mode
    , symbol              : {               // Activate the symbol mode
        dest              : '../../styles'
      , prefix            : '.symbol-%s'
      , dimensions        : true
      , sprite            : '../images/icons/icons.symbol.svg'
      , bust              : true
      , render            : {
          scss            : true                // Activate CSS output (with default options)
        }
      , example           : {
          dest            : '../images/icons/test.symbol.html'
        }
      , inline            : false
      }
    , stack               : false           // Activate the stack mode
    }
  };

  del.sync( [ 'src/images/icons' ] );
  gulp.src( 'src/images/_to-sprites/**/*.svg' )
    .pipe( $.plumber() )
    .pipe( $.svgSprite( config ) )
    .on( 'error', function( error ) {
        console.log( error );
    })
    .pipe( gulp.dest( 'src/images/icons' ) );
});

/*! 
 * Favicons generator for Gulp
 * https://github.com/haydenbleasel/gulp-favicons
 */
gulp.task( 'favicons', () => {
  del.sync( [ 'src/images/favicons/**' ] );
  require( 'fs' ).writeFile( 'src/_templates/components/_head/favicons.html', '', function( err ) {
    if ( err ) throw err;
  } );
  return gulp.src( packageJson.config.faviconPath )
    .pipe( $.favicons(
      {
        files: {
          src              : packageJson.config.faviconPath
        , dest             : 'images/favicons'
        , html             : 'src/_templates/components/_head/favicons.html'
        , iconsPath        : 'images/favicons'
        , androidManifest  : 'images/favicons'
        , browserConfig    : 'images/favicons'
        , firefoxManifest  : 'images/favicons'
        , yandexManifest   : 'images/favicons'
        }
      , icons: {
          android          : true
        , appleIcon        : true
        , appleStartup     : true
        , coast            : true
        , favicons         : true
        , firefox          : true
        , opengraph        : true
        , windows          : true
        , yandex           : true
      }
      , settings: {
          appName          : packageJson.config.projectName
        , appDescription   : packageJson.description
        , developer        : packageJson.author.name
        , developerURL     : packageJson.author.url
        , version          : packageJson.version
        , background       : packageJson.config.primaryColor
        , index            : 'index.html'
        , url              : packageJson.homepage
        , silhouette       : true
        , logging          : false
        }
      }, function ( error, metadata ) {
          if ( error ) {
              throw error;
          }
          console.log( metadata, 'Metadata produced during the build process' );
      } ) );
});



gulp.task( 'fonts:gen', () => {
  return gulp.src( 'src/fonts/**/*.{otf,ttf}' )
    .pipe( $.fontgen(
      {
        dest         : 'src/fonts'
      , css_fontpath : '../fonts'
      } ) );
});

gulp.task( 'fonts:concat', [ 'fonts:gen' ], () => {
  return gulp.src( 'src/fonts/**/*.css' )
    .pipe( $.concat( 'fonts.scss' ) )
    .pipe( gulp.dest( 'src/styles' ) );
});

gulp.task( 'fonts', [ 'fonts:concat' ], () => {
  return gulp.src( require( 'main-bower-files' )( {
    filter: '**/*.{eot,svg,ttf,woff,woff2}'
  } ).concat( 'src/fonts/**/*.{eot,svg,ttf,woff,woff2}' ) )
    .pipe( gulp.dest( '.tmp/fonts' ) )
    .pipe( gulp.dest( 'dist/fonts' ) );
});

gulp.task( 'extras', () => {
  return gulp.src( [
    'src/*.*',
    '!src/*.html'
  ], {
    dot: true
  } ).pipe( gulp.dest( 'dist' ) );
});

gulp.task( 'clean', del.bind( null, [
    '.tmp'
  , 'dist'
  , 'src/images/favicons/*'
  , 'src/images/sprites'
  ] ) );

gulp.task( 'serve', [ 'favicons', 'styles', 'fonts' ], () => {
  browserSync( {
    notify    : false,
    port      : 9000,
    ghostMode : false,
    // open      : 'tunnel',
    // tunnel    : 'sparxclouddev',
    // browser   : ['google chrome canary', 'chromium'],
    server    : {
      baseDir : [ '.tmp', 'src' ],
      routes  : {
        '/bower_components': 'bower_components'
      }
    }
  });

  gulp.watch( [
    'src/*.html',
    'src/scripts/**/*.js',
    'src/styles/**/*',
    'src/images/**/*',
    '.tmp/fonts/**/*'
  ] ).on( 'change', reload );

  gulp.watch( 'src/styles/**/*.scss', [ 'styles' ] );
  gulp.watch( 'src/fonts/**/*', [ 'fonts' ] );
  gulp.watch( 'bower.json', [ 'wiredep', 'fonts' ] );
  gulp.watch( 'src/_templates/**/*.html', [ 'templates' ] );
});

gulp.task( 'serve:dist', () => {
  browserSync( {
    notify    : false,
    port      : 9001,
    ghostMode : false,
    open      : 'tunnel',
    tunnel    : 'sparxcloud',
    server    : {
      baseDir : [ 'dist' ],
      middleware: [compress()]
    }
  });
});

gulp.task( 'serve:test', () => {
  browserSync( {
    notify: false,
    port: 9003,
    ui: false,
    server: {
      baseDir: 'test',
      routes: {
        '/bower_components': 'bower_components'
      }
    }
  });

  gulp.watch( 'test/spec/**/*.js' ).on( 'change', reload );
  gulp.watch( 'test/spec/**/*.js', [ 'lint:test' ] );
});

// inject bower components
gulp.task( 'wiredep', () => {
  gulp.src( 'src/styles/*.scss' )
    .pipe( wiredep( {
      ignorePath: /^(\.\.\/)+/
    } ) )
    .pipe( gulp.dest( 'src/styles' ) );

  gulp.src( 'src/_templates/**/*.html' )
    .pipe( wiredep( {
      exclude: [ 'bootstrap-sass' ],
      ignorePath: /^(\.\.\/)*\.\./
    } ) )
    .pipe( gulp.dest( 'src/_templates/' ) );
});

gulp.task( 'build', [ 'favicons', 'lint', 'html', 'images', 'fonts', 'extras' ], () => {
  return gulp.src( 'dist/**/*' )
    .pipe( $.size( { title: 'build', gzip: true } ) );
});

gulp.task( 'default', [ 'clean' ], () => {
  gulp.start( 'build' );
});
